<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Alexander Pushkin'
            ],
            [
                'name' => 'Alexander Solzhenitsyn'
            ],
            [
                'name' => 'Ivan Turgenev'
            ],
            [
                'name' => 'Vladimir Nabokov'
            ],
            [
                'name' => 'Mikhail Bulgakov'
            ],
            [
                'name' => 'Anton Chekhov'
            ],
            [
                'name' => 'Ivan Bunin'
            ],
            [
                'name' => 'Nikolai Gogol'
            ],
            [
                'name' => 'Fyodor Dostoyevsky'
            ],
            [
                'name' => 'Leo Tolstoy'
            ],
        ];

        Author::insert($data);
    }
}
