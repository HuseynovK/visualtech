<?php


namespace App\Services;


use App\Http\Resources\AuthorResource;
use App\Models\Author;
use Illuminate\Database\Eloquent\Model;

class AuthorService extends BaseServise
{
    public function __construct(Author $model)
    {
        parent::__construct($model);
    }

    public function getAll()
    {
        return AuthorResource::collection($this->model->orderByDesc('id')->get());
    }
}
