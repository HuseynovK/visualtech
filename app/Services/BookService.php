<?php


namespace App\Services;


use App\Http\Resources\BookResource;
use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;

class BookService extends BaseServise
{
    public function __construct(Book $model)
    {
        parent::__construct($model);
    }

    public function getAll()
    {
        return BookResource::collection($this->model->with('book_author')->where('user_id', request()->user()->id)->get());
    }

    public function getOne($book)
    {
        return (new BookResource($book->load('book_author')));
    }

    public function store($data)
    {
        $book = $this->model->create(['name' => $data['name'], 'user_id' => request()->user()->id]);
        $book->book_author()->attach($data['authors']);
        return (new BookResource($book->load('book_author')));
    }

    public function update($book, $data)
    {
        $book->update(['name' => $data['name']]);
        $book->book_author()->detach();
        $book->book_author()->attach($data['authors']);
        return (new BookResource($book->load('book_author')));
    }

    public function delete($book)
    {
        $book->book_author()->detach();
        $book->delete();
        return true;
    }
}
