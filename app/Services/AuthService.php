<?php


namespace App\Services;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService extends BaseServise
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function login($credentials)
    {
        $user = $this->model->where('email', $credentials['email'])->first();
        if ($user) {
            if (Hash::check($credentials['password'], $user->password)) {
                $token = $user->createToken(time() . "-" . $user->email)->plainTextToken;
                return ['token' => $token];
            }
        }
        return false;
    }

    public function register($data)
    {
        return $this->model->create($data);
    }
}
