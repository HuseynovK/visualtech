<?php


namespace App\Mixin;


use Illuminate\Http\Response;

class ResponceMixin
{
    public function send_only_status()
    {
        return function ($data) {
            return ['data' => $data];
        };
    }
    public function send_with_data()
    {
        return function ($data) {
            return ['data' => $data];
        };
    }

    public function success_with_data()
    {
        return function ($data) {
            return ['success' => true, 'data' => $data];
        };
    }

    public function success()
    {
        return function ($message, $url) {
            return ['success' => true, 'message' => $message, 'url' => $url];
        };
    }

    public function fail()
    {
        return function ($m, $message) {
            return ['error' => $m, 'message' => $message];
        };
    }
}
