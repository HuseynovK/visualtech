<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Services\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public $authServide;

    public function __construct(AuthService $authService)
    {
        $this->authServide = $authService;
    }

    public function login(LoginRequest $request)
    {
        return response()->success_with_data($this->authServide->login($request->validated()));
    }

    public function register(RegisterRequest $request)
    {
        return response()->success_with_data($this->authServide->register($request->validated()));
    }
}
