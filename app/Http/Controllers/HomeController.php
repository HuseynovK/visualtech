<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        $books = Book::orderByDesc('id')->with('book_author')->get();
        return view('welcome', compact('books'));
    }
}
